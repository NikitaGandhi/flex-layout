Copyright (c) 2016 by Yakov Khalinsky (http://codepen.io/yakovkhalinsky/pen/ONGMWj)

# Flex Layout Example
This application uses css flex to create a three row + 2 column inner layout.

# How to Run
Open index.html in any browser which renders the index page with above layout

Note: project css file is located in css/style.css folder.
